import java.io.*;
import java.util.*;
import java.text.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Note: Due to the format of the submissions, all classes had to be put into one file when each class should probably have
 * their own... i.e. I made a poor decision to do this in JAVA!
 * 
 * In general all searches are basically constant time except for the prefix search of content blocks in the XML.
 */
public class Solution
{
	/**
	 * Interface for all database types used in this submission.
	 */
	public interface Database
	{
		/**
		 * Take the given Document object and populate the database with the appropriate information.
		 * @param doc - the Document object containing the information from the XML that follows a CREATE command.
		 */
		public void create(Document doc);

		/**
		 * Take the given Document object that contains the information for a note (GUID) that already exists in the
		 * database and update the database with the new information.
		 * @param doc - the Document object containing the information from the XML that follows an UPDATE command.
		 */
		public void update(Document doc);

		/**
		 * Delete the given note (GUID) from the database.
		 * @param guid - the GUID of the note to delete from the database.
		 */
		public void delete(String guid);

		/**
		 * Retrieve the GUIDs that match the query.
		 * @param query - raw query (no "tag:"s or "*"s removed) to search for within the database.
		 * @return a HashSet<String> containing all the GUIDs that match the given query.
		 */
		public HashSet<String> search(String query);
	}

	/**
	 * Database to store all tag information as a whole string.  This database only supports exact tag searches.
	 */
	public static class ExactTagDatabase implements Database
	{
		// key:value pairs are tag:set(GUIDs)
		HashMap<String,HashSet<String>> exactTagDatabase = null;

		/**
		 * Constructor
		 */
		public ExactTagDatabase()
		{
			this.exactTagDatabase = new HashMap<String,HashSet<String>>();
		}

		@Override
		public void create(Document doc)
		{
			NodeList tagNodes = doc.getElementsByTagName("tag");
			String guid = doc.getElementsByTagName("guid").item(0).getTextContent();

			// for loop to process each tag element found in the given XML file
			for(int i = 0; i < tagNodes.getLength(); i++)
			{
				Node tagNode = tagNodes.item(i);
				String tag = tagNode.getTextContent().toLowerCase();
				HashSet<String> guidSet = null;

				// if the tag already exists in the database, add the GUID to the existing GUID set for the tag; otherwise
				// create and store a new pair of tag:set(GUIDs) in the database
				if(this.exactTagDatabase.containsKey(tag))
				{
					guidSet = this.exactTagDatabase.get(tag);
					guidSet.add(guid);
				}
				else
				{
					guidSet = new HashSet<String>();
					guidSet.add(guid);
					this.exactTagDatabase.put(tag, guidSet);
				}
			}
		}

		@Override
		public void update(Document doc)
		{
			// an update is basically just a delete followed by a create
			String guid = doc.getElementsByTagName("guid").item(0).getTextContent();
			this.delete(guid);
			this.create(doc);
		}

		@Override
		public void delete(String guid)
		{
			HashSet<String> guidSet = null;

			// for loop to remove the given GUID from any tags that the GUID belongs to 
			for(String tag : this.exactTagDatabase.keySet())
			{
				guidSet = this.exactTagDatabase.get(tag);
				guidSet.remove(guid);
			}
		}

		@Override
		public HashSet<String> search(String query)
		{
			// remove the "tag:" portion from the search query and do a simple lookup
			String queryTrimmed = query.substring(4);
			return new HashSet<String>(this.exactTagDatabase.get(queryTrimmed));
		}
	}

	/**
	 * Database to store all prefix substrings of the tags.  This database only supports tag prefix searches.
	 * Note: Prefix searches for words (from content block of XML) used to have a separate class similar to this one, but
	 *		 this implementation was too slow as the 4th test case would time out.
	 */
	public static class PrefixTagDatabase implements Database
	{
		// key:value pairs are tag-prefix:set(GUIDs)
		HashMap<String,HashSet<String>> prefixTagDatabase = null;

		/**
		 * Constructor
		 */
		public PrefixTagDatabase()
		{
			this.prefixTagDatabase = new HashMap<String,HashSet<String>>();
		}

		@Override
		public void create(Document doc)
		{
			NodeList tagNodes = doc.getElementsByTagName("tag");
			String guid = doc.getElementsByTagName("guid").item(0).getTextContent();

			// for loop to process each tag element found in the given XML file
			for(int i = 0; i < tagNodes.getLength(); i++)
			{
				Node tagNode = tagNodes.item(i);
				String tag = tagNode.getTextContent().toLowerCase();

				// for loop to process each prefix substring of the tags found
				for(int endIndex = 1; endIndex <= tag.length(); endIndex++)
				{
					HashSet<String> guidSet = null;
					String prefix = tag.substring(0, endIndex);

					// if the tag prefix already exists in the database add the GUID to the existing GUID set for the tag
					// prefix; otherwise create and store a new pair of tag-prefix:set(GUIDs) in the database
					if(this.prefixTagDatabase.containsKey(prefix))
					{
						guidSet = this.prefixTagDatabase.get(prefix);
						guidSet.add(guid);
					}
					else
					{
						guidSet = new HashSet<String>();
						guidSet.add(guid);
						this.prefixTagDatabase.put(prefix, guidSet);
					}
				}
			}
		}

		@Override
		public void update(Document doc)
		{
			// an update is basically just a delete followed by a create
			String guid = doc.getElementsByTagName("guid").item(0).getTextContent();
			this.delete(guid);
			this.create(doc);
		}

		@Override
		public void delete(String guid)
		{
			HashSet<String> guidSet = null;

			// for loop to remove the given GUID from any tag prefixes that the GUID belongs to
			for(String tag : this.prefixTagDatabase.keySet())
			{
				guidSet = this.prefixTagDatabase.get(tag);
				guidSet.remove(guid);
			}
		}

		@Override
		public HashSet<String> search(String query)
		{
			// remove the "tag:" and "*" portions from the search query and do a simple lookup
			int endIndex = query.length();
			String queryTrimmed = query.substring(4, endIndex - 1);
			return new HashSet<String>(this.prefixTagDatabase.get(queryTrimmed));
		}
	}

	/**
	 * Database to store all word information (from content block of XML) as a whole string.  This database supports exact
	 * word searches as well as word prefix searches.
	 */
	public static class TextDatabase implements Database
	{
		// key:value pairs are word:set(GUIDs)
		HashMap<String,HashSet<String>> textDatabase = null;

		/**
		 * Constructor
		 */
		public TextDatabase()
		{
			this.textDatabase = new HashMap<String,HashSet<String>>();
		}

		@Override
		public void create(Document doc)
		{
			Node contentNode = doc.getElementsByTagName("content").item(0);
			String guid = doc.getElementsByTagName("guid").item(0).getTextContent();
			String rawContentText = contentNode.getTextContent();
			// clean up content text by removing extra whitespace and replace any unimportant characters with spaces
			String cleanedContentText = rawContentText.trim().replaceAll("[^a-zA-Z0-9']", " ").replaceAll(" +", " ");
			String[] words = cleanedContentText.split(" ");

			// for loop to process each word found in the content block of the given XML file
			for(int i = 0; i < words.length; i++)
			{
				String word = words[i].toLowerCase();
				HashSet<String> guidSet = null;

				// if the word already exists in the database add the GUID to the existing GUID set for the word; otherwise
				// create and store a new pair of word:set(GUIDs) in the database
				if(this.textDatabase.containsKey(word))
				{
					guidSet = this.textDatabase.get(word);
					guidSet.add(guid);
				}
				else
				{
					guidSet = new HashSet<String>();
					guidSet.add(guid);
					this.textDatabase.put(word, guidSet);
				}
			}
		}

		@Override
		public void update(Document doc)
		{
			// an update is basically just a delete followed by a create
			String guid = doc.getElementsByTagName("guid").item(0).getTextContent();
			this.delete(guid);
			this.create(doc);
		}

		@Override
		public void delete(String guid)
		{
			HashSet<String> guidSet = null;

			// for loop to remove the given GUID from any words that the GUID belongs to
			for(String word : this.textDatabase.keySet())
			{
				guidSet = this.textDatabase.get(word);
				guidSet.remove(guid);
			}
		}

		/**
		 * Method to perform prefix searching of the words found in the content block of the XML files stored in the
		 * database.
		 * @param prefix - the prefix to search for (with "*" removed from the end of the search term)
		 * @return a HashSet<String> containing all the GUIDs that match the given query.
		 */
		private HashSet<String> prefixSearch(String prefix)
		{
			HashSet<String> guidSet = new HashSet<String>();

			// for loop to build up the set of GUIDs for notes that match the given prefix by checking that the word matches
			// the given prefix
			for(String word : this.textDatabase.keySet())
			{
				if(word.startsWith(prefix))
				{
					guidSet.addAll(this.textDatabase.get(word));
				}
			}

			return guidSet;
		}

		@Override
		public HashSet<String> search(String query)
		{
			// if a prefix search is given use the prefixSearch method, removing the ending "*"; otherwise do a simple
			// lookup
			if(query.endsWith("*"))
			{
				return this.prefixSearch(query.substring(0, query.length() - 1));
			}
			else
			{
				return new HashSet<String>(this.textDatabase.get(query));
			}
		}
	}

	/**
	 * Database to store all creation times for each note (GUID).
	 */
	public static class CreationTimeDatabase implements Database
	{
		// key:value pairs are GUID:timestamp; where timestamp is the ISO 8601 timestamp
		HashMap<String,String> creationTimeDatabase = null;

		/**
		 * Constructor
		 */
		public CreationTimeDatabase()
		{
			this.creationTimeDatabase = new HashMap<String,String>();
		}

		@Override
		public void create(Document doc)
		{
			// store the creation time for each GUID as key:value pairs GUID:timestamp
			Node contentNode = doc.getElementsByTagName("created").item(0);
			String guid = doc.getElementsByTagName("guid").item(0).getTextContent();
			String date = contentNode.getTextContent();
			this.creationTimeDatabase.put(guid, date);
		}

		@Override
		public void update(Document doc)
		{
			// since put() overwrites the existing value for a key:value pair, can just call create() method
			this.create(doc);
		}

		@Override
		public void delete(String guid)
		{
			// do a simple key delete
			this.creationTimeDatabase.remove(guid);
		}

		@Override
		public HashSet<String> search(String query)
		{
			String searchParameterDate = query.split(":")[1];
			HashSet<String> guidSet = new HashSet<String>();
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

			// for loop to find all notes (GUIDs) that have a creation date >= the query date
			for(String guid : this.creationTimeDatabase.keySet())
			{
				String rawDate = this.creationTimeDatabase.get(guid);
				String[] extractedDateComponents = rawDate.split("T")[0].split("-");
				String extractedDate = extractedDateComponents[0] + 
										extractedDateComponents[1] +
										extractedDateComponents[2];
				try
				{
					Date databaseDate = dateFormat.parse(extractedDate);
					Date queryDate = dateFormat.parse(searchParameterDate);
					if(databaseDate.after(queryDate) || databaseDate.equals(queryDate))
					{
						guidSet.add(guid);
					}
				}
				catch (ParseException e)
				{
					e.printStackTrace();
				}
			}

			return guidSet;
		}

		/**
		 * Inner class to hold a GUID and its associated creation date.
		 */
		private class GuidToDateHolder
		{
			public String guid;
			public Date date;

			/**
			 * Constructor
			 * @param guid - the GUID of the note.
			 * @param date - the associated creation date for the GUID passed to the constructor.
			 */
			public GuidToDateHolder(String guid, Date date)
			{
				this.guid = guid;
				this.date = date;
			}
		}

		/**
		 * Custom Comparator to order GuidToDateHolder objects so that GUIDs can be printed from earliest to latest.
		 */
		private class GuidToDateHolderComparator implements Comparator<GuidToDateHolder>
		{
			@Override
			public int compare(GuidToDateHolder firstPair, GuidToDateHolder secondPair)
			{
				return firstPair.date.compareTo(secondPair.date);
			}
		}

		/**
		 * Take the given set of GUIDs and return them in sorted order (earliest to latest).
		 * @param guidSet - the set of GUIDs to return in sorted order (earliest to latest).
		 * @return a csv of the given set of GUIDs in sorted order (earliest to latest).
		 */
		public String sortGuidByDateSearch(Set<String> guidSet)
		{
			// the sorted GUIDs for a null set or empty set is nothing
			if(guidSet == null || guidSet.isEmpty())
			{
				return "";
			}

			DateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
			ArrayList<GuidToDateHolder> guidToDateHolders = new ArrayList<GuidToDateHolder>();

			// for loop to create a list of GuidToDateHolder objects, one GuidToDateHolder for each GUID given
			for(Object guid : guidSet.toArray())
			{

				String rawTimeStamp = this.creationTimeDatabase.get(guid);
				try
				{
					Date fullTimeStamp = timeStampFormat.parse(rawTimeStamp);
					guidToDateHolders.add(new GuidToDateHolder((String) guid, fullTimeStamp));
				}
				catch (ParseException e)
				{
					e.printStackTrace();
				}
			}

			// sort the list of GuidToDateHolder objects using the custom comparator GuidToDateHolderComparator
			Collections.sort(guidToDateHolders, new GuidToDateHolderComparator());

			StringBuilder guidCsvBuilder = new StringBuilder();

			// for loop to create a csv of the GUIDs in sorted order (earliest to latest)
			for(GuidToDateHolder guidToDateHolder : guidToDateHolders)
			{
				guidCsvBuilder = guidCsvBuilder.append("," + guidToDateHolder.guid);
			}

			return guidCsvBuilder.toString().substring(1);
		}
	}
	
	/**
	 * Helper method to create a temporary XML file from the given input stream.  The provided BufferedReader must contain
	 * a line only with "</note>".
	 * @param documentBuilder - the DocumentBuilder used to generate a Document object from the generated File object.
	 * @param fileStream - the buffered filestream containing the XML information to extract.
	 * @return a Document object that contains the XML information of a note block.
	 * @throws IOException if there an any IO issues when creating the temporary XML file. 
	 * @throws SAXException if there is a problem parsing the File object into a Document object. 
	 */
	private static Document createFile(DocumentBuilder documentBuilder,
										BufferedReader fileStream) throws IOException, SAXException
	{
		File xmlFile = new File("temp.xml");
		FileWriter fileWriter = new FileWriter(xmlFile);
		BufferedWriter xmlWriter = new BufferedWriter(fileWriter);
		String currentLine = "";

		// while loop to read from the buffered filestream until the end of the note block is reached
		while(! currentLine.equals("</note>"))
		{
			currentLine = fileStream.readLine().replaceAll("&", "&amp;");
			xmlWriter.write(currentLine);
		}

		// close the file writer and delete the temporary file once the program quits
		xmlWriter.close();
		xmlFile.deleteOnExit();

		return documentBuilder.parse(xmlFile);
	}

	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException
	{
		String filePath = "C:\\Users\\Richard\\Desktop\\test.input1.txt";
		FileInputStream inputFile = new FileInputStream(new File(filePath));
		BufferedReader fileStream = new BufferedReader(new InputStreamReader(inputFile));
		String command = null;

		// setup databases
		ArrayList<Database> specializedDatabases = new ArrayList<Database>();
		Database exactTagDatabase = new ExactTagDatabase();
		Database prefixTagDatabase = new PrefixTagDatabase();
		Database textDatabase = new TextDatabase();
		CreationTimeDatabase creationTimeDatabase = new CreationTimeDatabase();
		specializedDatabases.add(exactTagDatabase);
		specializedDatabases.add(prefixTagDatabase);
		specializedDatabases.add(textDatabase);
		specializedDatabases.add(creationTimeDatabase);

		// setup objects to assist with processing XML information
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

		while(true)
		{
			command = fileStream.readLine();

			// exit while loop once all input has been processed
			if(command == null) break;

			// determine which command is being executed and executed it for the appropriate database(s)
			if(command.equals("CREATE"))
			{
				Document creationInfo = createFile(documentBuilder, fileStream);

				for(Database database : specializedDatabases)
				{
					database.create(creationInfo);
				}
			}
			else if(command.equals("UPDATE"))
			{
				Document updateInfo = createFile(documentBuilder, fileStream);

				for(Database database : specializedDatabases)
				{
					database.update(updateInfo);
				}
			}
			else if(command.equals("DELETE"))
			{
				String guid = fileStream.readLine();

				for(Database database : specializedDatabases)
				{
					database.delete(guid);
				}
			}
			else if(command.equals("SEARCH"))
			{
				String[] queries = fileStream.readLine().split(" ");
				HashSet<String> finalGuidSet = null;

				// for loop to process each query in the search, producing an intersection of all the search results
				for(String query : queries)
				{
					HashSet<String> temporaryGuidSet = null;

					if(query.startsWith("tag:") && query.endsWith("*"))
					{
						temporaryGuidSet = prefixTagDatabase.search(query);
					}
					else if(query.startsWith("tag:"))
					{
						temporaryGuidSet = exactTagDatabase.search(query);
					}
					else if(query.startsWith("created:"))
					{
						temporaryGuidSet = creationTimeDatabase.search(query);
					}
					else
					{
						temporaryGuidSet = textDatabase.search(query);
					}

					if(temporaryGuidSet != null)
					{
						if(finalGuidSet == null)
						{
							finalGuidSet = temporaryGuidSet;
						}
						else
						{
							finalGuidSet.retainAll(temporaryGuidSet);
						}
					}
				}

				// print out the GUIDs found to match the search term(s) in sorted order (earliest to latest)
				System.out.println(creationTimeDatabase.sortGuidByDateSearch(finalGuidSet));
			}
			else
			{
				System.out.println("Unknown command given!");
			}
		}
	}
}